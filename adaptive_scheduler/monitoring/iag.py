"""
iag.py - Retrieve and identify changes in the weather status of the IAG telescopes.

Author: Tim-Oliver Husser
May 2021
"""
import requests
import logging

from adaptive_scheduler.monitoring.monitors import NetworkStateMonitor, Event


log = logging.getLogger(__name__)


WEATHER_STATIONS = {
    '0m5a.roof.goe': 'https://weather.iag50srv.astro.physik.uni-goettingen.de',
    '1m2a.roof.cpt': 'https://weather.monet.saao.ac.za',
}


class IAGMonitor(NetworkStateMonitor):
    """ Monitor weather status of IAG telescopes. """

    def __init__(self, configdb_interface):
        super(IAGMonitor, self).__init__(configdb_interface=configdb_interface)

    def retrieve_data(self):
        # loop all weather stations
        data = []
        for name, url in WEATHER_STATIONS.items():
            try:
                # get data from weather station
                r = requests.get(url + '/api/current/', timeout=10)

                # add it
                data.append(dict(telescope=name, weather=r.json()))

            except requests.RequestException:
                # something went bad, just add telescope without weather
                data.append(dict(telescope=name))

        # return list of data
        return data

    def create_event(self, datum):
        event = Event(
            type="BADWEATHER",
            reason="Bad weather",
            start_time=None,
            end_time=None,
        )

        return event

    def create_resource(self, datum):
        return datum['telescope']

    def is_an_event(self, datum):
        try:
            for sensor, status in datum['weather']['sensors'].items():
                # if we find any sensor other than sunalt with good=False, we return True
                if sensor != 'sunalt' and status['good'] is False:
                    return True
            # no bad sensor found
            return False

        except KeyError:
            return True
